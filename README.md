# Spaceport Releases

An archive of all spaceport releases beginning with version 0.2.1-Alpha.

The way we hash and sign releases is subject to change in the future. A copy of the README.md at the time of release beginning below the divider is included in the release folder to document what the procedure used at that time was.

-------

### Version 0.2.2-Alpha

releases.json in the root directory contains the version and signature for the latest release

File format of releases.json is
{
"version string" : { "address" : "signed message" }
}
where signed message is the version string signed by the address

Each release version folder will contain the following files
- the binaries for that release
- hashes.txt containing the sha256sum result for each binary
- signatures.json containing signatures for signing the hashes.txt file
- keys.txt containing a list of keys of our team members

To validate your download, check that the hash matches the hash in the hashes.txt file for that release. Then check that the signature for that hashes.txt file is valid.
